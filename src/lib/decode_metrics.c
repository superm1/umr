/*
 * Copyright 2022 Advanced Micro Devices, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Authors: Tom St Denis <tom.stdenis@amd.com>
 *
 */
#include "umr.h"
#include <stddef.h>

#define METRICS_HEADER_LIST(__FIELD)	\
	__FIELD(structure_size),	\
	__FIELD(format_revision),	\
	__FIELD(content_revision),

#define METRICS_INFO_V1_0_LIST(__FIELD) \
	__FIELD(system_clock_counter),	\
	__FIELD(temperature_edge),	\
	__FIELD(temperature_hotspot),	\
	__FIELD(temperature_mem),	\
	__FIELD(temperature_vrgfx),	\
	__FIELD(temperature_vrsoc),	\
	__FIELD(temperature_vrmem),	\
	__FIELD(average_gfx_activity),	\
	__FIELD(average_umc_activity),	\
	__FIELD(average_mm_activity),	\
	__FIELD(average_socket_power),	\
	__FIELD(energy_accumulator),	\
	__FIELD(average_gfxclk_frequency),	\
	__FIELD(average_socclk_frequency),	\
	__FIELD(average_uclk_frequency),	\
	__FIELD(average_vclk0_frequency),	\
	__FIELD(average_dclk0_frequency),	\
	__FIELD(average_vclk1_frequency),	\
	__FIELD(average_dclk1_frequency),	\
	__FIELD(current_gfxclk),	\
	__FIELD(current_socclk),	\
	__FIELD(current_uclk),	\
	__FIELD(current_vclk0),	\
	__FIELD(current_dclk0),	\
	__FIELD(current_vclk1),	\
	__FIELD(current_dclk1),	\
	__FIELD(throttle_status),	\
	__FIELD(current_fan_speed),	\
	__FIELD(pcie_link_width),	\
	__FIELD(pcie_link_speed),

#define METRICS_INFO_V1_1_LIST(__FIELD) \
	__FIELD(temperature_edge),	\
	__FIELD(temperature_hotspot),	\
	__FIELD(temperature_mem),	\
	__FIELD(temperature_vrgfx),	\
	__FIELD(temperature_vrsoc),	\
	__FIELD(temperature_vrmem),	\
	__FIELD(average_gfx_activity),	\
	__FIELD(average_umc_activity),	\
	__FIELD(average_mm_activity),	\
	__FIELD(average_socket_power),	\
	__FIELD(energy_accumulator),	\
	__FIELD(system_clock_counter),	\
	__FIELD(average_gfxclk_frequency),	\
	__FIELD(average_socclk_frequency),	\
	__FIELD(average_uclk_frequency),	\
	__FIELD(average_vclk0_frequency),	\
	__FIELD(average_dclk0_frequency),	\
	__FIELD(average_vclk1_frequency),	\
	__FIELD(average_dclk1_frequency),	\
	__FIELD(current_gfxclk),	\
	__FIELD(current_socclk),	\
	__FIELD(current_uclk),	\
	__FIELD(current_vclk0),	\
	__FIELD(current_dclk0),	\
	__FIELD(current_vclk1),	\
	__FIELD(current_dclk1),	\
	__FIELD(throttle_status),	\
	__FIELD(current_fan_speed),	\
	__FIELD(pcie_link_width),	\
	__FIELD(pcie_link_speed),

#define METRICS_INFO_V1_2_LIST(__FIELD) \
	__FIELD(temperature_edge),	\
	__FIELD(temperature_hotspot),	\
	__FIELD(temperature_mem),	\
	__FIELD(temperature_vrgfx),	\
	__FIELD(temperature_vrsoc),	\
	__FIELD(temperature_vrmem),	\
	__FIELD(average_gfx_activity),	\
	__FIELD(average_umc_activity),	\
	__FIELD(average_mm_activity),	\
	__FIELD(average_socket_power),	\
	__FIELD(energy_accumulator),	\
	__FIELD(system_clock_counter),	\
	__FIELD(average_gfxclk_frequency),	\
	__FIELD(average_socclk_frequency),	\
	__FIELD(average_uclk_frequency),	\
	__FIELD(average_vclk0_frequency),	\
	__FIELD(average_dclk0_frequency),	\
	__FIELD(average_vclk1_frequency),	\
	__FIELD(average_dclk1_frequency),	\
	__FIELD(current_gfxclk),	\
	__FIELD(current_socclk),	\
	__FIELD(current_uclk),	\
	__FIELD(current_vclk0),	\
	__FIELD(current_dclk0),	\
	__FIELD(current_vclk1),	\
	__FIELD(current_dclk1),	\
	__FIELD(throttle_status),	\
	__FIELD(current_fan_speed),	\
	__FIELD(pcie_link_width),	\
	__FIELD(pcie_link_speed),       \
	__FIELD(padding), \
	__FIELD(gfx_activity_acc), \
	__FIELD(mem_activity_acc), \
	__FIELD(temperature_hbm[0]), \
	__FIELD(temperature_hbm[1]), \
	__FIELD(temperature_hbm[2]), \
	__FIELD(temperature_hbm[3]), \
	__FIELD(firmware_timestamp),


#define METRICS_INFO_V1_3_LIST(__FIELD) \
	__FIELD(temperature_edge),	\
	__FIELD(temperature_hotspot),	\
	__FIELD(temperature_mem),	\
	__FIELD(temperature_vrgfx),	\
	__FIELD(temperature_vrsoc),	\
	__FIELD(temperature_vrmem),	\
	__FIELD(average_gfx_activity),	\
	__FIELD(average_umc_activity),	\
	__FIELD(average_mm_activity),	\
	__FIELD(average_socket_power),	\
	__FIELD(energy_accumulator),	\
	__FIELD(system_clock_counter),	\
	__FIELD(average_gfxclk_frequency),	\
	__FIELD(average_socclk_frequency),	\
	__FIELD(average_uclk_frequency),	\
	__FIELD(average_vclk0_frequency),	\
	__FIELD(average_dclk0_frequency),	\
	__FIELD(average_vclk1_frequency),	\
	__FIELD(average_dclk1_frequency),	\
	__FIELD(current_gfxclk),	\
	__FIELD(current_socclk),	\
	__FIELD(current_uclk),	\
	__FIELD(current_vclk0),	\
	__FIELD(current_dclk0),	\
	__FIELD(current_vclk1),	\
	__FIELD(current_dclk1),	\
	__FIELD(throttle_status),	\
	__FIELD(current_fan_speed),	\
	__FIELD(pcie_link_width),	\
	__FIELD(pcie_link_speed),       \
	__FIELD(padding), \
	__FIELD(gfx_activity_acc), \
	__FIELD(mem_activity_acc), \
	__FIELD(temperature_hbm[0]), \
	__FIELD(temperature_hbm[1]), \
	__FIELD(temperature_hbm[2]), \
	__FIELD(temperature_hbm[3]), \
	__FIELD(firmware_timestamp), \
	__FIELD(voltage_soc), \
	__FIELD(voltage_gfx), \
	__FIELD(voltage_mem), \
	__FIELD(padding1), \
	__FIELD(indep_throttle_status),

#define METRICS_INFO_V1_4_LIST(__FIELD) \
	__FIELD(temperature_hotspot),	\
	__FIELD(temperature_mem),	\
	__FIELD(temperature_vrsoc),	\
	__FIELD(curr_socket_power),	\
	__FIELD(average_gfx_activity),	\
	__FIELD(average_umc_activity),	\
	__FIELD(vcn_activity[0]),	\
	__FIELD(vcn_activity[1]),	\
	__FIELD(vcn_activity[2]),	\
	__FIELD(vcn_activity[3]),	\
	__FIELD(energy_accumulator),	\
	__FIELD(system_clock_counter),	\
	__FIELD(throttle_status),	\
	__FIELD(gfxclk_lock_status),	\
	__FIELD(pcie_link_width),	\
	__FIELD(pcie_link_speed),       \
	__FIELD(xgmi_link_width),	\
	__FIELD(xgmi_link_speed),	\
	__FIELD(gfx_activity_acc), \
	__FIELD(mem_activity_acc), \
	__FIELD(pcie_bandwidth_acc),	\
	__FIELD(pcie_bandwidth_inst),	\
	__FIELD(pcie_l0_to_recov_count_acc),	\
	__FIELD(pcie_replay_count_acc),	\
	__FIELD(pcie_replay_rover_count_acc),	\
	__FIELD(xgmi_read_data_acc[0]),	\
	__FIELD(xgmi_read_data_acc[1]),	\
	__FIELD(xgmi_read_data_acc[2]),	\
	__FIELD(xgmi_read_data_acc[3]),	\
	__FIELD(xgmi_read_data_acc[4]),	\
	__FIELD(xgmi_read_data_acc[5]),	\
	__FIELD(xgmi_read_data_acc[6]),	\
	__FIELD(xgmi_read_data_acc[7]),	\
	__FIELD(xgmi_write_data_acc[0]),	\
	__FIELD(xgmi_write_data_acc[1]),	\
	__FIELD(xgmi_write_data_acc[2]),	\
	__FIELD(xgmi_write_data_acc[3]),	\
	__FIELD(xgmi_write_data_acc[4]),	\
	__FIELD(xgmi_write_data_acc[5]),	\
	__FIELD(xgmi_write_data_acc[6]),	\
	__FIELD(xgmi_write_data_acc[7]),	\
	__FIELD(firmware_timestamp),	\
	__FIELD(current_gfxclk[0]),	\
	__FIELD(current_gfxclk[1]),	\
	__FIELD(current_gfxclk[2]),	\
	__FIELD(current_gfxclk[3]),	\
	__FIELD(current_gfxclk[4]),	\
	__FIELD(current_gfxclk[5]),	\
	__FIELD(current_gfxclk[6]),	\
	__FIELD(current_gfxclk[7]),	\
	__FIELD(current_socclk[0]),	\
	__FIELD(current_socclk[1]),	\
	__FIELD(current_socclk[2]),	\
	__FIELD(current_socclk[3]),	\
	__FIELD(current_vclk0[0]),	\
	__FIELD(current_vclk0[1]),	\
	__FIELD(current_vclk0[2]),	\
	__FIELD(current_vclk0[3]),	\
	__FIELD(current_dclk0[0]),	\
	__FIELD(current_dclk0[1]),	\
	__FIELD(current_dclk0[2]),	\
	__FIELD(current_dclk0[3]),	\
	__FIELD(current_uclk),	\
	__FIELD(padding),

#define METRICS_INFO_V2_0_LIST(__FIELD) \
	__FIELD(system_clock_counter),	\
	__FIELD(temperature_gfx),	\
	__FIELD(temperature_soc),	\
	__FIELD(temperature_core[0]),	\
	__FIELD(temperature_core[1]),	\
	__FIELD(temperature_core[2]),	\
	__FIELD(temperature_core[3]),	\
	__FIELD(temperature_core[4]),	\
	__FIELD(temperature_core[5]),	\
	__FIELD(temperature_core[6]),	\
	__FIELD(temperature_core[7]),	\
	__FIELD(temperature_l3[0]),	\
	__FIELD(temperature_l3[1]),	\
	__FIELD(average_gfx_activity),	\
	__FIELD(average_mm_activity),	\
	__FIELD(average_socket_power),	\
	__FIELD(average_cpu_power),	\
	__FIELD(average_soc_power),	\
	__FIELD(average_gfx_power),	\
	__FIELD(average_core_power[0]),	\
	__FIELD(average_core_power[1]),	\
	__FIELD(average_core_power[2]),	\
	__FIELD(average_core_power[3]),	\
	__FIELD(average_core_power[4]),	\
	__FIELD(average_core_power[5]),	\
	__FIELD(average_core_power[6]),	\
	__FIELD(average_core_power[7]),	\
	__FIELD(average_gfxclk_frequency),	\
	__FIELD(average_socclk_frequency),	\
	__FIELD(average_uclk_frequency),	\
	__FIELD(average_fclk_frequency),	\
	__FIELD(average_vclk_frequency),	\
	__FIELD(average_dclk_frequency),	\
	__FIELD(current_gfxclk),	\
	__FIELD(current_socclk),	\
	__FIELD(current_uclk),	\
	__FIELD(current_fclk),	\
	__FIELD(current_vclk),	\
	__FIELD(current_dclk),	\
	__FIELD(current_coreclk[0]),	\
	__FIELD(current_coreclk[1]),	\
	__FIELD(current_coreclk[2]),	\
	__FIELD(current_coreclk[3]),	\
	__FIELD(current_coreclk[4]),	\
	__FIELD(current_coreclk[5]),	\
	__FIELD(current_coreclk[6]),	\
	__FIELD(current_coreclk[7]),	\
	__FIELD(current_l3clk[0]),	\
	__FIELD(current_l3clk[1]),	\
	__FIELD(throttle_status),	\
	__FIELD(fan_pwm),

#define METRICS_INFO_V2_1_LIST(__FIELD) \
	__FIELD(temperature_gfx),	\
	__FIELD(temperature_soc),	\
	__FIELD(temperature_core[0]),	\
	__FIELD(temperature_core[1]),	\
	__FIELD(temperature_core[2]),	\
	__FIELD(temperature_core[3]),	\
	__FIELD(temperature_core[4]),	\
	__FIELD(temperature_core[5]),	\
	__FIELD(temperature_core[6]),	\
	__FIELD(temperature_core[7]),	\
	__FIELD(temperature_l3[0]),	\
	__FIELD(temperature_l3[1]),	\
	__FIELD(average_gfx_activity),	\
	__FIELD(average_mm_activity),	\
	__FIELD(system_clock_counter),	\
	__FIELD(average_socket_power),	\
	__FIELD(average_cpu_power),	\
	__FIELD(average_soc_power),	\
	__FIELD(average_gfx_power),	\
	__FIELD(average_core_power[0]),	\
	__FIELD(average_core_power[1]),	\
	__FIELD(average_core_power[2]),	\
	__FIELD(average_core_power[3]),	\
	__FIELD(average_core_power[4]),	\
	__FIELD(average_core_power[5]),	\
	__FIELD(average_core_power[6]),	\
	__FIELD(average_core_power[7]),	\
	__FIELD(average_gfxclk_frequency),	\
	__FIELD(average_socclk_frequency),	\
	__FIELD(average_uclk_frequency),	\
	__FIELD(average_fclk_frequency),	\
	__FIELD(average_vclk_frequency),	\
	__FIELD(average_dclk_frequency),	\
	__FIELD(current_gfxclk),	\
	__FIELD(current_socclk),	\
	__FIELD(current_uclk),	\
	__FIELD(current_fclk),	\
	__FIELD(current_vclk),	\
	__FIELD(current_dclk),	\
	__FIELD(current_coreclk[0]),	\
	__FIELD(current_coreclk[1]),	\
	__FIELD(current_coreclk[2]),	\
	__FIELD(current_coreclk[3]),	\
	__FIELD(current_coreclk[4]),	\
	__FIELD(current_coreclk[5]),	\
	__FIELD(current_coreclk[6]),	\
	__FIELD(current_coreclk[7]),	\
	__FIELD(current_l3clk[0]),	\
	__FIELD(current_l3clk[1]),	\
	__FIELD(throttle_status),	\
	__FIELD(fan_pwm),

#define METRICS_INFO_V2_2_LIST(__FIELD) \
	__FIELD(temperature_gfx),	\
	__FIELD(temperature_soc),	\
	__FIELD(temperature_core[0]),	\
	__FIELD(temperature_core[1]),	\
	__FIELD(temperature_core[2]),	\
	__FIELD(temperature_core[3]),	\
	__FIELD(temperature_core[4]),	\
	__FIELD(temperature_core[5]),	\
	__FIELD(temperature_core[6]),	\
	__FIELD(temperature_core[7]),	\
	__FIELD(temperature_l3[0]),	\
	__FIELD(temperature_l3[1]),	\
	__FIELD(average_gfx_activity),	\
	__FIELD(average_mm_activity),	\
	__FIELD(system_clock_counter),	\
	__FIELD(average_socket_power),	\
	__FIELD(average_cpu_power),	\
	__FIELD(average_soc_power),	\
	__FIELD(average_gfx_power),	\
	__FIELD(average_core_power[0]),	\
	__FIELD(average_core_power[1]),	\
	__FIELD(average_core_power[2]),	\
	__FIELD(average_core_power[3]),	\
	__FIELD(average_core_power[4]),	\
	__FIELD(average_core_power[5]),	\
	__FIELD(average_core_power[6]),	\
	__FIELD(average_core_power[7]),	\
	__FIELD(average_gfxclk_frequency),	\
	__FIELD(average_socclk_frequency),	\
	__FIELD(average_uclk_frequency),	\
	__FIELD(average_fclk_frequency),	\
	__FIELD(average_vclk_frequency),	\
	__FIELD(average_dclk_frequency),	\
	__FIELD(current_gfxclk),	\
	__FIELD(current_socclk),	\
	__FIELD(current_uclk),	\
	__FIELD(current_fclk),	\
	__FIELD(current_vclk),	\
	__FIELD(current_dclk),	\
	__FIELD(current_coreclk[0]),	\
	__FIELD(current_coreclk[1]),	\
	__FIELD(current_coreclk[2]),	\
	__FIELD(current_coreclk[3]),	\
	__FIELD(current_coreclk[4]),	\
	__FIELD(current_coreclk[5]),	\
	__FIELD(current_coreclk[6]),	\
	__FIELD(current_coreclk[7]),	\
	__FIELD(current_l3clk[0]),	\
	__FIELD(current_l3clk[1]),	\
	__FIELD(throttle_status),	\
	__FIELD(fan_pwm), \
	__FIELD(padding[0]), \
	__FIELD(padding[1]), \
	__FIELD(padding[2]), \
	__FIELD(indep_throttle_status),

#define METRICS_INFO_V2_3_LIST(__FIELD) \
	__FIELD(temperature_gfx),	\
	__FIELD(temperature_soc),	\
	__FIELD(temperature_core[0]),	\
	__FIELD(temperature_core[1]),	\
	__FIELD(temperature_core[2]),	\
	__FIELD(temperature_core[3]),	\
	__FIELD(temperature_core[4]),	\
	__FIELD(temperature_core[5]),	\
	__FIELD(temperature_core[6]),	\
	__FIELD(temperature_core[7]),	\
	__FIELD(temperature_l3[0]),	\
	__FIELD(temperature_l3[1]),	\
	__FIELD(average_gfx_activity),	\
	__FIELD(average_mm_activity),	\
	__FIELD(system_clock_counter),	\
	__FIELD(average_socket_power),	\
	__FIELD(average_cpu_power),	\
	__FIELD(average_soc_power),	\
	__FIELD(average_gfx_power),	\
	__FIELD(average_core_power[0]),	\
	__FIELD(average_core_power[1]),	\
	__FIELD(average_core_power[2]),	\
	__FIELD(average_core_power[3]),	\
	__FIELD(average_core_power[4]),	\
	__FIELD(average_core_power[5]),	\
	__FIELD(average_core_power[6]),	\
	__FIELD(average_core_power[7]),	\
	__FIELD(average_gfxclk_frequency),	\
	__FIELD(average_socclk_frequency),	\
	__FIELD(average_uclk_frequency),	\
	__FIELD(average_fclk_frequency),	\
	__FIELD(average_vclk_frequency),	\
	__FIELD(average_dclk_frequency),	\
	__FIELD(current_gfxclk),	\
	__FIELD(current_socclk),	\
	__FIELD(current_uclk),	\
	__FIELD(current_fclk),	\
	__FIELD(current_vclk),	\
	__FIELD(current_dclk),	\
	__FIELD(current_coreclk[0]), \
	__FIELD(current_coreclk[1]), \
	__FIELD(current_coreclk[2]), \
	__FIELD(current_coreclk[3]), \
	__FIELD(current_coreclk[4]), \
	__FIELD(current_coreclk[5]), \
	__FIELD(current_coreclk[6]), \
	__FIELD(current_coreclk[7]), \
	__FIELD(current_l3clk[0]), \
	__FIELD(current_l3clk[1]), \
	__FIELD(throttle_status),	\
	__FIELD(fan_pwm), \
	__FIELD(padding[0]), \
	__FIELD(padding[1]), \
	__FIELD(padding[2]), \
	__FIELD(indep_throttle_status), \
	__FIELD(average_temperature_gfx), \
	__FIELD(average_temperature_soc), \
	__FIELD(average_temperature_core[0]), \
	__FIELD(average_temperature_core[1]), \
	__FIELD(average_temperature_core[2]), \
	__FIELD(average_temperature_core[3]), \
	__FIELD(average_temperature_core[4]), \
	__FIELD(average_temperature_core[5]), \
	__FIELD(average_temperature_core[6]), \
	__FIELD(average_temperature_core[7]), \
	__FIELD(average_temperature_l3[0]), \
	__FIELD(average_temperature_l3[1])

#define METRICS_INFO_V2_4_LIST(__FIELD) \
	__FIELD(temperature_gfx),	\
	__FIELD(temperature_soc),	\
	__FIELD(temperature_core[0]),	\
	__FIELD(temperature_core[1]),	\
	__FIELD(temperature_core[2]),	\
	__FIELD(temperature_core[3]),	\
	__FIELD(temperature_core[4]),	\
	__FIELD(temperature_core[5]),	\
	__FIELD(temperature_core[6]),	\
	__FIELD(temperature_core[7]),	\
	__FIELD(temperature_l3[0]),	\
	__FIELD(temperature_l3[1]),	\
	__FIELD(average_gfx_activity),	\
	__FIELD(average_mm_activity),	\
	__FIELD(system_clock_counter),	\
	__FIELD(average_socket_power),	\
	__FIELD(average_cpu_power),	\
	__FIELD(average_soc_power),	\
	__FIELD(average_gfx_power),	\
	__FIELD(average_core_power[0]),	\
	__FIELD(average_core_power[1]),	\
	__FIELD(average_core_power[2]),	\
	__FIELD(average_core_power[3]),	\
	__FIELD(average_core_power[4]),	\
	__FIELD(average_core_power[5]),	\
	__FIELD(average_core_power[6]),	\
	__FIELD(average_core_power[7]),	\
	__FIELD(average_gfxclk_frequency),	\
	__FIELD(average_socclk_frequency),	\
	__FIELD(average_uclk_frequency),	\
	__FIELD(average_fclk_frequency),	\
	__FIELD(average_vclk_frequency),	\
	__FIELD(average_dclk_frequency),	\
	__FIELD(current_gfxclk),	\
	__FIELD(current_socclk),	\
	__FIELD(current_uclk),	\
	__FIELD(current_fclk),	\
	__FIELD(current_vclk),	\
	__FIELD(current_dclk),	\
	__FIELD(current_coreclk[0]), \
	__FIELD(current_coreclk[1]), \
	__FIELD(current_coreclk[2]), \
	__FIELD(current_coreclk[3]), \
	__FIELD(current_coreclk[4]), \
	__FIELD(current_coreclk[5]), \
	__FIELD(current_coreclk[6]), \
	__FIELD(current_coreclk[7]), \
	__FIELD(current_l3clk[0]), \
	__FIELD(current_l3clk[1]), \
	__FIELD(throttle_status),	\
	__FIELD(fan_pwm), \
	__FIELD(padding[0]), \
	__FIELD(padding[1]), \
	__FIELD(padding[2]), \
	__FIELD(indep_throttle_status), \
	__FIELD(average_temperature_gfx), \
	__FIELD(average_temperature_soc), \
	__FIELD(average_temperature_core[0]), \
	__FIELD(average_temperature_core[1]), \
	__FIELD(average_temperature_core[2]), \
	__FIELD(average_temperature_core[3]), \
	__FIELD(average_temperature_core[4]), \
	__FIELD(average_temperature_core[5]), \
	__FIELD(average_temperature_core[6]), \
	__FIELD(average_temperature_core[7]), \
	__FIELD(average_temperature_l3[0]), \
	__FIELD(average_temperature_l3[1]), \
	__FIELD(average_cpu_voltage), \
	__FIELD(average_soc_voltage), \
	__FIELD(average_gfx_voltage), \
	__FIELD(average_cpu_current), \
	__FIELD(average_soc_current), \
	__FIELD(average_gfx_current)


static struct field_info metrics_header[] = {
#define METRICS_HEADER_INFO(MEMBER)	FIELD_INFO(struct umr_metrics_table_header, MEMBER)
	METRICS_HEADER_LIST(METRICS_HEADER_INFO)
};

static struct field_info metrics_v1_0[] = {
#define METRICS_V1_0_INFO(MEMBER)	FIELD_INFO(struct umr_gpu_metrics_v1_0, MEMBER)
	METRICS_INFO_V1_0_LIST(METRICS_V1_0_INFO)
};

static struct field_info metrics_v1_1[] = {
#define METRICS_V1_1_INFO(MEMBER)	FIELD_INFO(struct umr_gpu_metrics_v1_1, MEMBER)
	METRICS_INFO_V1_1_LIST(METRICS_V1_1_INFO)
};

static struct field_info metrics_v1_2[] = {
#define METRICS_V1_2_INFO(MEMBER)	FIELD_INFO(struct umr_gpu_metrics_v1_2, MEMBER)
	METRICS_INFO_V1_2_LIST(METRICS_V1_2_INFO)
};

static struct field_info metrics_v1_3[] = {
#define METRICS_V1_3_INFO(MEMBER)	FIELD_INFO(struct umr_gpu_metrics_v1_3, MEMBER)
	METRICS_INFO_V1_3_LIST(METRICS_V1_3_INFO)
};

static struct field_info metrics_v1_4[] = {
#define METRICS_V1_4_INFO(MEMBER)	FIELD_INFO(struct umr_gpu_metrics_v1_4, MEMBER)
	METRICS_INFO_V1_4_LIST(METRICS_V1_4_INFO)
};

static struct field_info metrics_v2_0[] = {
#define METRICS_V2_0_INFO(MEMBER)	FIELD_INFO(struct umr_gpu_metrics_v2_0, MEMBER)
	METRICS_INFO_V2_0_LIST(METRICS_V2_0_INFO)
};

static struct field_info metrics_v2_1[] = {
#define METRICS_V2_1_INFO(MEMBER)	FIELD_INFO(struct umr_gpu_metrics_v2_1, MEMBER)
	METRICS_INFO_V2_1_LIST(METRICS_V2_1_INFO)
};

static struct field_info metrics_v2_2[] = {
#define METRICS_V2_2_INFO(MEMBER)	FIELD_INFO(struct umr_gpu_metrics_v2_2, MEMBER)
	METRICS_INFO_V2_2_LIST(METRICS_V2_2_INFO)
};

static struct field_info metrics_v2_3[] = {
#define METRICS_V2_3_INFO(MEMBER)	FIELD_INFO(struct umr_gpu_metrics_v2_3, MEMBER)
	METRICS_INFO_V2_3_LIST(METRICS_V2_3_INFO)
};

static struct field_info metrics_v2_4[] = {
#define METRICS_V2_4_INFO(MEMBER)	FIELD_INFO(struct umr_gpu_metrics_v2_4, MEMBER)
	METRICS_INFO_V2_4_LIST(METRICS_V2_4_INFO)
};

static void umr_dump_field_info(struct umr_asic *asic, const struct field_info *info,
				const uint32_t count, const char *prefix, const uint8_t *ref, int delay)
{
	uint32_t i;
	const struct field_info *tmp;
	const char *fmt = "%s%-30s = %lld\n";

	if (!prefix)
		prefix = "";

	if (delay) {
		struct timespec currentTime;
		time_t seconds;
		long milliseconds;
		struct tm localTime;
		char timebuf[256];

		clock_gettime(CLOCK_REALTIME, &currentTime);
		seconds = currentTime.tv_sec;
		milliseconds = currentTime.tv_nsec / 1000000;
		localtime_r(&seconds, &localTime);

		strftime(timebuf, sizeof timebuf, "%d-%m-%Y %H:%M:%S", &localTime);
		sprintf(timebuf + strlen(timebuf), ".%ld,", milliseconds);

		if (delay > 0) {
			asic->std_msg("time,");
			for (i = 0, tmp = &info[i]; i < count; i++, tmp = &info[i]) {
				asic->std_msg("%s,", tmp->name);
			}
			asic->std_msg("\n");
		}

		asic->std_msg("%s,", timebuf);
		for (i = 0, tmp = &info[i]; i < count; i++, tmp = &info[i]) {
			switch (tmp->size) {
			case 1:
				asic->std_msg("%lld,", *(uint8_t *)(ref + tmp->offset));
				break;
			case 2:
				asic->std_msg("%lld,", *(uint16_t *)(ref + tmp->offset));
				break;
			case 4:
				asic->std_msg("%lld,", *(uint32_t *)(ref + tmp->offset));
				break;
			case 8:
				asic->std_msg("%lld,", *(uint64_t *)(ref + tmp->offset));
				break;
			default:
				break;
			}
		}
		asic->std_msg("\n");
	} else {
		for (i = 0, tmp = &info[i]; i < count; i++, tmp = &info[i]) {
			switch (tmp->size) {
			case 1:
				asic->std_msg(fmt, prefix, tmp->name, *(uint8_t *)(ref + tmp->offset));
				break;
			case 2:
				asic->std_msg(fmt, prefix, tmp->name, *(uint16_t *)(ref + tmp->offset));
				break;
			case 4:
				asic->std_msg(fmt, prefix, tmp->name, *(uint32_t *)(ref + tmp->offset));
				break;
			case 8:
				asic->std_msg(fmt, prefix, tmp->name, *(uint64_t *)(ref + tmp->offset));
				break;
			default:
				break;
			}
		}
	}
}

int umr_dump_metrics(struct umr_asic *asic, const void *table, uint32_t size, int delay)
{
	struct umr_metrics_table_header *header =
		(struct umr_metrics_table_header *)table;

	if (!table || !size)
		return -1;

	if (!delay)
		umr_dump_field_info(asic, metrics_header, ARRAY_SIZE(metrics_header), " hdr.", table, delay);

#define METRICS_VERSION(a, b)	((a << 16) | b )

	switch (METRICS_VERSION(header->format_revision, header->content_revision)) {
	case METRICS_VERSION(1, 0):
		umr_dump_field_info(asic, metrics_v1_0, ARRAY_SIZE(metrics_v1_0), "v1_0.", table, delay);
		break;
	case METRICS_VERSION(1, 1):
		umr_dump_field_info(asic, metrics_v1_1, ARRAY_SIZE(metrics_v1_1), "v1_1.", table, delay);
		break;
	case METRICS_VERSION(1, 2):
		umr_dump_field_info(asic, metrics_v1_2, ARRAY_SIZE(metrics_v1_2), "v1_2.", table, delay);
		break;
	case METRICS_VERSION(1, 3):
		umr_dump_field_info(asic, metrics_v1_3, ARRAY_SIZE(metrics_v1_3), "v1_3.", table, delay);
		break;
	case METRICS_VERSION(1, 4):
		umr_dump_field_info(asic, metrics_v1_4, ARRAY_SIZE(metrics_v1_4), "v1_4.", table, delay);
		break;
	case METRICS_VERSION(2, 0):
		umr_dump_field_info(asic, metrics_v2_0, ARRAY_SIZE(metrics_v2_0), "v2_0.", table, delay);
		break;
	case METRICS_VERSION(2, 1):
		umr_dump_field_info(asic, metrics_v2_1, ARRAY_SIZE(metrics_v2_1), "v2_1.", table, delay);
		break;
	case METRICS_VERSION(2, 2):
		umr_dump_field_info(asic, metrics_v2_2, ARRAY_SIZE(metrics_v2_2), "v2_2.", table, delay);
		break;
	case METRICS_VERSION(2, 3):
		umr_dump_field_info(asic, metrics_v2_3, ARRAY_SIZE(metrics_v2_3), "v2_3.", table, delay);
		break;
	case METRICS_VERSION(2, 4):
		umr_dump_field_info(asic, metrics_v2_4, ARRAY_SIZE(metrics_v2_4), "v2_4.", table, delay);
		break;
	default:
		asic->err_msg("[ERROR]: Unknown Metrics table format: 0x%"PRIx8"\n", header->format_revision);
		return -1;
	}

	return 0;
}

